
from datetime import datetime
from time import sleep

print "\nStarting ..."

initial_time = datetime.today()

counter = 0

while True:
  sleep(1)
  counter = counter + 1
  current_time = datetime.today()
  print "Current date/time: ", current_time
  if counter > 5:
    break

print "Done.\n"


